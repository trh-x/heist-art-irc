import styled, { createGlobalStyle } from 'styled-components/macro';
import { COLOR, FONT } from './theme';

export const GlobalStyle = createGlobalStyle`
  html, body, #root {
    height: 100%;
    ${({ showPreferDesktop }) => showPreferDesktop || `
      min-width: 1550px;
    `}
  }
  body {
    background: ${COLOR.defaultBackground};
    color: ${COLOR.defaultText};
    font-family: ${FONT.defaultFamily};
    input, textarea {
      padding: 0.5rem;
    }
  }
  canvas {
    outline: none;
  }

  .Resizer {
    background: #000;
    opacity: 1;
    box-sizing: border-box;
    background-clip: padding-box;
  }

  .Resizer.horizontal {
    height: 11px;
    margin: -1px 0;
    border-top: 1px solid rgba(26, 103, 94, 0);
    border-bottom: 5px solid rgba(255, 255, 255, 0);
    cursor: row-resize;
    width: 100%;
  }

  .Pane:hover + .Resizer.horizontal,
  .Resizer.horizontal:hover {
    transition: all 2s ease;
    border-top: 1px solid rgba(26, 103, 94, 0.5);
    border-bottom: 5px solid rgba(0, 0, 0, 0.5);
  }

  div.dg {
    font-family: monospace;
    text-transform: lowercase;

    input, select {
      font-family: monospace;
      text-transform: lowercase;
    }

    &.ac {
      z-index: 10;
      top: 115px;

      .close-button {
        color: gray;
        // font-family: monospace;
        // text-transform: lowercase;
      }
    }
  }
`;

export const Main = styled.main`
  margin: 0 auto;
  width: 100%;
  height: 100%;
  position: relative;
`;

export const PreferDesktop = styled.div`
  margin: 10px;
  line-height: 15px;
`;
// export const TopToggle = styled.a`
//   position: fixed;
//   top: 0;
//   right: 0;
//   z-index: 20;
// `;

export const Section = styled.section`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const TopSection = styled(Section)`
  display: flex;
  flex-wrap: nowrap;
`;

export const CanvasWrapper = styled.div`
  position: relative;
  width: 100%;
  margin-right: auto;
  ${({ maxWidth }) => `
    max-width: ${maxWidth}
  `}
`;

export const RegisterLink = styled.a`
  white-space: nowrap;
  color: #0F0;
  margin-top: 10px;
  margin-right: 20px;
  font-size: 22px;
`;

export const InstructionalText = styled.span`
  line-height: 20px;
  margin-top: 10px;
  margin-right: 10px;
  font-size: 12px;
`;

export const Link = styled.a`
  text-decoration: none;
  position: fixed;
  top: 62px;
  right: 16px;
  color: grey;
  font-size: 12px;
`;
