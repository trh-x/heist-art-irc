import React, { useCallback, useEffect, useRef, useState } from 'react';
import SplitPane from 'react-split-pane';
import { Reset } from 'styled-reset';
import { Terminal } from '@techanvil/xterm-react-renderer'

import Banner from './Banner';
import { GlobalStyle, Main, PreferDesktop, Section, RegisterLink, TopSection, CanvasWrapper, InstructionalText, Link } from './App.styled';

import textModel from './font/tlwg-mono/mytetra-heist.json';

const initialMaxWidth = 700; // 1340;
const startDate = new Date('December 10, 2020 00:00:00');

const showLive = window.location.hash === '#live';

const isMobile = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
const shouldShowPreferDesktop = () => isMobile() && window.innerWidth < 1550;

const Instructions = () => {
  const [hasStarted, setHasStarted] = useState(Date.now() > startDate.getTime());

  if (hasStarted || showLive) return (
    <InstructionalText>
      Change your nick with /nick myname.
      <br />
      Click on the chat, and use Page Up and Page Down to scroll.
      <br />
      Best viewed in full-screen.
    </InstructionalText>
  );

  setTimeout(() => {
    setHasStarted(true);
  }, startDate.getTime() - Date.now());

  return <RegisterLink href="https://heistart.com/signup" target="_blank">register now</RegisterLink>;
};

function App() {
  const [maxWidth, setMaxWidth] = useState(`${initialMaxWidth}px`);
  const onDragFinished = newHeight => setMaxWidth(`${newHeight / 80 * initialMaxWidth}px`);

  const [showPreferDesktop, setShowPreferDesktop] = useState(shouldShowPreferDesktop());
  console.log('showPreferDesktop', showPreferDesktop);
  useEffect(() => {
    window.addEventListener('resize', () => {
      setShowPreferDesktop(shouldShowPreferDesktop());
    });
  }, []);

  return <>
    <Reset />
    <GlobalStyle showPreferDesktop={showPreferDesktop} />
    <Main>
      { showPreferDesktop
        ? <PreferDesktop>mytetra/HEIST<br /><br />This site is designed for a desktop experience. Please switch to a desktop browser.</PreferDesktop>
        : (
      <SplitPane split="horizontal" minSize={80} defaultSize={80} onDragFinished={onDragFinished}>
        <TopSection>
          <CanvasWrapper maxWidth={maxWidth}>
            <Banner
              camera={{ position: [0, 0.5, 0], zoom: 100 }}
              text="mytetra/HEIST" model={textModel} />
          </CanvasWrapper>
          <Instructions />
          <Link href="https://terminal.ninja" target="_blank">?</Link>
        </TopSection>
        <Section>
          <Terminal theme={{foreground: '#0F0'}} fontSize={11} />
        </Section>
      </SplitPane>
        )}
    </Main>
  </>;
}

export default App;
