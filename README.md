# Heist Chat

`irssi` in your browser. :)

A mashup of Three.JS and an SSH terminal, using react-three-fiber and xterm.js / webssh2.

[heistart.com](https://heistart.com)
